//
// Copyright (C) INRIA Serge Steer <Serge.Steer@inria.fr>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
function mmwrite(filename,A,comment,field,precision)
    //Writes the sparse or dense matrix A to a Matrix Market (MM) formatted file.

    [lhs, rhs] = argn()
    apifun_checkrhs ( "mmwrite" , rhs , 2:5 )
    apifun_checklhs ( "mmwrite" , lhs , 0:1 )

    //
    // Manage input argument
    if type(A)==4 then
        A=bool2s(A)
    end
    if and(type(A)<>[1 5 6]) then
        error(msprintf(gettext("%s: not yet implemented for this array type"),"mmwrite"))
    end

    select rhs
    case 4 then
        precision = 16;
    case 3 then
        field = "default";  //will be determined by type of A
        precision = 16;
    case 2 then
        comment = [];
        field = "default";  //will be determined by type of A
        precision = 16;
    end;

    if comment==[] then
        comment="%% Generated by Scilab "+date()
    end

    //
    // Check types
    apifun_checktype ( "mmwrite" , filename ,  "filename" , 1 , "string" )
    apifun_checktype ( "mmwrite" , A ,  "A" , 2 , ["constant" "boolean" "boolean sparse" "sparse"] )
    apifun_checktype ( "mmwrite" , comment ,  "comment" , 3 , "string" )
    apifun_checktype ( "mmwrite" , field ,  "field" , 4 , "string" )
    apifun_checktype ( "mmwrite" , precision ,  "precision" , 5 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "mmwrite" , filename , "filename" , 1 )
    apifun_checkvector ( "mmwrite" , comment ,  "comment" , 3 , size(comment,"*") )
    apifun_checkscalar ( "mmwrite" , field ,  "field" , 4 )
    apifun_checkscalar ( "mmwrite" , precision ,  "precision" , 5 )
    //
    // Check content
    apifun_checkgreq ( "mmwrite" , precision ,  "precision" , 5 , 1 )
    if ( precision <> floor(precision) ) then
        error(msprintf(gettext("%s: Wrong value for input argument #%d: An integer value expected.\n"),5))
    end
    //
    // Proceed...
    mmfile = mopen(filename,"w");

    [M,N] = size(A);
    if field=='default' then
        if type(A)==6 then
            field='pattern'
        elseif ~isreal(A) then
            field='complex'
        else
            field='real'
        end
    end

    //%%%%%%%%%%%%       This part for sparse matrices     %%%%%%%%%%%%%%%%
    if or(type(A)==[5 6]) then
        symm = "general";
        if M == N then // square matrix, Determine symmetry:
            [IJ , V ] = spget(tril(A  , -1));
            [IJt, Vt] = spget(tril(A.', -1));
            if and(IJ == IJt) then
                //upper and lower triangle have same non zero elements positions
                if V == Vt then
                    symm = "symmetric";
                    [IJ,V] = spget(tril(A));
                    NZ=size(V,'*')
                elseif V == -Vt then
                    symm = "skew-symmetric";
                    [IJ,V] = spget(tril(A))
                    NZ=size(V,'*')
                elseif field == 'complex' ..
                     & V == conj(Vt) ..
                     & isreal(diag(A), 0)  then
                    symm = "hermitian";
                    [IJ,V] = spget(tril(A))
                    NZ=size(V,'*')
                end
            end
        end
        if symm == "general" then
            [IJ,V] = spget(A);
        end
        NZ=size(V,'*')

        mfprintf(mmfile,"%%%%MatrixMarket matrix %s %s %s\n", "coordinate",field,symm);
        for line = comment' // workaround for cases where it printed only the first comment (mmwrite.tst on scilab 6.0.0-alpha-1 on linux)
            mfprintf(mmfile,"%%%s\n",line);
        end

        mfprintf(mmfile,"%d %d %d\n",M,N,NZ);
        cplxformat = msprintf("%%d %%d %% .%dg %% .%dg",precision,precision)+'\n';
        realformat = msprintf("%%d %%d %% .%dg",precision)+'\n';
        select field
        case "real" then
            mfprintf(mmfile,realformat,IJ,V(:));
        case "complex" then
            mfprintf(mmfile,cplxformat,IJ,real(V(:)),imag(V(:)));
        case "pattern" then
            mfprintf(mmfile,"%d %d\n",IJ);
        else
            error("Unsupported field type:"+field)
        end;

        //%%%%%%%%%%%%       This part for dense matrices      %%%%%%%%%%%%%%%%
    elseif type(A)==1 then
        symm = "general";
        if M==N then //square matrix; check for symmetry
            if and(A==A.') then
                symm = "symmetric";
            elseif and(triu(A,1)==-tril(A,-1).') then
                symm = "skew-symmetric";
            elseif field=="complex" & and(A==A') then
                symm = "hermitian";
            end
        end

        mfprintf(mmfile,"%%%%MatrixMarket matrix %s %s %s\n","array",field,symm);
        mfprintf(mmfile,"%%%s\n",comment);
        mfprintf(mmfile,"%d %d\n",M,N);
        cplxformat = msprintf("%% .%dg %% .%dg",precision,precision)+'\n';
        realformat = msprintf("%% .%dg\n",precision)+'\n';
        select field
        case "real" then
            if symm=="general" then
                mfprintf(mmfile,realformat,A(:));
            else
                for j = 1:N
                    mfprintf(mmfile,realformat,A(j:$,j));
                end;
            end
        case "complex" then
            if symm=="general" then
                mfprintf(mmfile,cplxformat,real(A(:)),imag(A(:)));
            else
                for j = 1:N
                    mfprintf(mmfile,cplxformat,real(A(j:$,j)),imag(A(j:$,j)));
                end;
            end
        case "pattern" then
            error("Pattern type inconsistant with dense matrix")
        else
            error("Unsupported field type:"+field)
        end
    end
    mclose(mmfile);
endfunction
