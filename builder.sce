// ====================================================================
// Copyright (C) 2009 - INRIA - Serge Steer
// Copyright (C) 2011 - Benoit Goepfert
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function buildmymodule()

	mode(-1)
	lines(0)
	
	try
	 getversion("scilab");
	catch
	 error(gettext("Scilab 5.0 or more is required."));  
	end;
	
	// ====================================================================
	if ~with_module("development_tools") then
	  error(msprintf(gettext("%s module not installed."),"development_tools"));
	end
	
	// ====================================================================
	TOOLBOX_NAME  = "MatrixMarket";
	TOOLBOX_TITLE = "Matmark";
	// ====================================================================
	
	toolbox_path  = get_absolute_file_path("builder.sce");
	tbx_builder_macros(toolbox_path);
	tbx_builder_help(toolbox_path);
	tbx_build_loader(TOOLBOX_NAME,toolbox_path);
	tbx_build_cleaner(TOOLBOX_NAME,toolbox_path);

endfunction

buildmymodule();
clear buildmymodule;

