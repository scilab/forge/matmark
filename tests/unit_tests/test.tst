// ====================================================================
// Copyright (C) 2009 - INRIA - Serge Steer
// Copyright (C) 2011 - Benoit Goepfert
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
//
// <-- JVM NOT MANDATORY -->

path = mmgetpath (  );
path = fullfile(path,"tests","unit_tests");
//Dense storage
//---------------

//1 - Real case

//1.1 general
A=matrix(1:12,3,4);
mmwrite(fullfile(TMPDIR,"A.mtx"),A) ; 
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"array");
assert_checkequal(field,"real");
assert_checkequal(symm,"general");
assert_checkequal(A1,A);


//1.2 symetric
A=A'*A;
mmwrite(fullfile(TMPDIR,"A.mtx"),A); 
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"array");
assert_checkequal(field,"real");
assert_checkequal(symm,"symmetric");
assert_checkequal(A1,A);

//1.3 skewsymetric
A=A-2*triu(A,1);
mmwrite(fullfile(TMPDIR,"A.mtx"),A)  ;
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"array");
assert_checkequal(field,"real");
assert_checkequal(symm,"skew-symmetric");
assert_checkequal(A1,A);

//2 Complex case

//2.1 general
A=matrix(1:12,3,4);
A(1,2)=A(1,2)+%i;
mmwrite(fullfile(TMPDIR,"A.mtx"),A) ; 
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"array");
assert_checkequal(field,"complex");
assert_checkequal(symm,"general");
assert_checkequal(A1,A);

//2.2 symetric
A=A.'*A;
mmwrite(fullfile(TMPDIR,"A.mtx"),A);  
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"array");
assert_checkequal(field,"complex");
assert_checkequal(symm,"symmetric");
assert_checkequal(A1,A);


//2.3 skewsymetric
A=A-2*triu(A,1);
mmwrite(fullfile(TMPDIR,"A.mtx"),A)  ;
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"array");
assert_checkequal(field,"complex");
assert_checkequal(symm,"skew-symmetric");
assert_checkequal(A1,A);



//2.4 hermitian
A=matrix(1:12,3,4);
A(1,2)=A(1,2)+%i;
A=A'*A;
mmwrite(fullfile(TMPDIR,"A.mtx"),A);  
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"array");
assert_checkequal(field,"complex");
assert_checkequal(symm,"hermitian");
assert_checkequal(A1,A);


//Sparse storage
//---------------

//1 - Real case

//1.1 general
A=sparse(matrix([0,0,3,4,0,0,0,8,9,0,11,0],3,4));
mmwrite(fullfile(TMPDIR,"A.mtx"),A) ; 
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"real");
assert_checkequal(symm,"general");
assert_checkequal(A1,A);

mmwrite(fullfile(TMPDIR,"A.mtx"),A,[],'pattern')
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
A2=A;
A2(find(A2<>0))=1;
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"pattern");
assert_checkequal(symm,"general");
assert_checkequal(A1,A2);

//1.2 symetric
A=A'*A;
mmwrite(fullfile(TMPDIR,"A.mtx"),A);  
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"real");
assert_checkequal(symm,"symmetric");
assert_checkequal(A1,A);

mmwrite(fullfile(TMPDIR,"A.mtx"),A,[],'pattern');  
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
A2=A;
A2(find(A2<>0))=1;
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"pattern");
assert_checkequal(symm,"symmetric");
assert_checkequal(A1,A2);


//1.3 skewsymetric
A=A-2*triu(A,1);
mmwrite(fullfile(TMPDIR,"A.mtx"),A)  ;
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"real");
assert_checkequal(symm,"skew-symmetric");
assert_checkequal(A1,A);

mmwrite(fullfile(TMPDIR,"A.mtx"),A,[],'pattern')  ;
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
A2=A;
A2(find(A>0))=1;A2(find(A<0))=-1;
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"pattern");
assert_checkequal(symm,"skew-symmetric");
assert_checkequal(A1,A2);


//2 Complex case

//2.1 general
A=sparse(matrix([0,0,3,4,0,0,0,8,9,0,11,0],3,4));
A(1,2)=A(1,2)+%i;
mmwrite(fullfile(TMPDIR,"A.mtx"),A) ; 
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"complex");
assert_checkequal(symm,"general");
assert_checkequal(A1,A);


//2.2 symetric
A=A.'*A;
mmwrite(fullfile(TMPDIR,"A.mtx"),A);  
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"complex");
assert_checkequal(symm,"symmetric");
assert_checkequal(A1,A);

//2.3 skewsymetric
A=A-2*triu(A,1);
mmwrite(fullfile(TMPDIR,"A.mtx"),A)  ;
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"complex");
assert_checkequal(symm,"skew-symmetric");
assert_checkequal(A1,A);



//2.4 hermitian
A=sparse(matrix([0,0,3,4,0,0,0,8,9,0,11,0],3,4));
A=A'*A;
A(1,3)=A(1,3)+%i;
A(3,1)=A(3,1)-%i;
mmwrite(fullfile(TMPDIR,"A.mtx"),A);  
[A1,rows,cols,entries,rep,field,symm] = mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(rep,"coordinate");
assert_checkequal(field,"complex");
assert_checkequal(symm,"hermitian");
assert_checkequal(A1,A);

//--------------------------------------------------------------------------------------------
//test with files downloaded from http://math.nist.gov/MatrixMarket

F=listfiles(fullfile(path,"*.mtx"));
for f=F'
  [rows,cols,entries,rep,field,symm,comments] = mminfo(f);
  A = mmread(f);
end

//--------------------------------------------------------------------------------------------


// Check value provided here:
// http://math.nist.gov/MatrixMarket/data/SPARSKIT/tokamak/utm300.html

[rows,cols,entries,rep,field,symm,comments] = mminfo(fullfile(path,"utm300.mtx"));
assert_checkequal(rows,300);
assert_checkequal(cols,300);
assert_checkequal(entries,3155);
assert_checkequal(field,"real");
assert_checkequal(symm,"general");

A=mmread(fullfile(path,"utm300.mtx"));

mmwrite(fullfile(TMPDIR,"A.mtx"),A,[],'real')
A1=mmread(fullfile(TMPDIR,"A.mtx"));
assert_checkequal(A1,A);

[nl,nc] = size(A);
assert_checkequal(nl,300);
assert_checkequal(nc,300);
assert_checkequal(nnz(A),3155);

// Diagonal
assert_checkequal(nnz(diag(A)),300);

// A-A'
assert_checkequal(nnz(A-A'),4382);

// Heaviest diagonals
offsets = [ 0 -50 50 1 -1 -5 5 2 -2 -4 ];
nonzerosExpected = [ 300 190 186 180 166 159 158 150 134 113 ];

for j=1:length(offsets)
	assert_checkequal(nnz(diag(A, offsets(j))),nonzerosExpected(j));
end

// TODO add the rest of information

[ij]=spget(A);
e=ij(:,1)-ij(:,2);

// Bandwidths average |i-j|
m=ceil(mean(abs(e)));
assert_checkequal(m,21);

// Bandwidths std.dev.
d=ceil(st_deviation(abs(e)));
assert_checkequal(d,23);

lastinrow=[find(diff(ij(:,1))>0) size(ij,1)];
rowlength=diff([0 lastinrow]);

// Row Data
[longest,indlongest]=max(rowlength);
assert_checkequal(longest,33);
assert_checkequal(indlongest,116);

[shortest,indshortest]=min(rowlength);
assert_checkequal(shortest,1);
assert_checkequal(indshortest,3);

// Average nonzeros per row 
m=ceil(mean(rowlength));
assert_checkequal(m,11);

d=round(st_deviation(rowlength)*10)/10;
assert_checkequal(d,7.7);

// Column Data
ji=gsort(ij(:,[2 1]),'lr','i');
lastincol=[find(diff(ji(:,1))>0) size(ji,1)];
collength=diff([0 lastincol]);

[longest,indlongest]=max(collength);
assert_checkequal(longest,22);
assert_checkequal(indlongest,59);

[shortest,indshortest]=min(collength);
assert_checkequal(shortest,1);
assert_checkequal(indshortest,16);

m=ceil(mean(collength));
assert_checkequal(m,11);

d=round(st_deviation(collength)*10)/10;
assert_checkequal(d,7.1);

// Profile Storage
lastinrow=[find(diff(ij(:,1))>0) size(ij,1)];
rowbounds= [ij([1 lastinrow(1:$-1)+1],2) ij(lastinrow,2)];
rowindex=ij(lastinrow,1);
rowbandwidth=rowbounds-rowindex*ones(1,2);
lowband=[-rowbandwidth(find(rowbandwidth(:,1)<=0),1)
         -rowbandwidth(find(rowbandwidth(:,2)<=0),2)];
uppband=[rowbandwidth(find(rowbandwidth(:,1)>=0),1)
         rowbandwidth(find(rowbandwidth(:,2)>=0),2)];   

// lower bandwidth => max
minlow=max(lowband);
assert_checkequal(minlow,74);

// lower bandwidth => min
maxlow=min(lowband);
assert_checkequal(maxlow,0);

// upper bandwidth => max
maxupp=max(uppband);
assert_checkequal(maxupp,66);

// upper bandwidth => min
minupp=min(uppband);
assert_checkequal(minupp,0);

