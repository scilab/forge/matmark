//
// Copyright (C) 2009 - INRIA - Serge Steer
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- JVM NOT MANDATORY -->

A=matrix(1:12,3,4);
filename = fullfile(TMPDIR,"A.mtx");
mmwrite(filename,A);
content=mgetl(filename);
expected = [
"%%MatrixMarket matrix array real general";
"%%% Generated by Scilab 27-Apr-2011";
"3 4";
" 1";
" 2";
" 3";
" 4";
" 5";
" 6";
" 7";
" 8";
" 9";
" 10";
" 11";
" 12"
];
// Do no compare line #2
assert_checkequal(content([1 3:14]),expected([1 3:14]));
//
A=sparse(matrix([0,0,3,4,0,0,0,8,9,0,11,0],3,4));
A=A'*A;
filename = fullfile(TMPDIR,"A1.mtx");
mmwrite(filename,A,["Test mmwrite";"Generated by Scilab"]) ;
content=mgetl(filename);
expected = [
"%%MatrixMarket matrix coordinate real symmetric";
"%Test mmwrite";
"%Generated by Scilab";
"4 4 6";
"1 1  9";
"2 2  16";
"3 1  27";
"3 3  145";
"4 3  88";
"4 4  121"
];

assert_checkequal(content,expected);

