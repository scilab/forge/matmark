<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
 * 
 * Copyright (C) 2010 - INRIA - Serge Steer
 * Copyright (C) 2011 - Benoit Goepfert
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
-->

<refentry xmlns="http://docbook.org/ns/docbook" 
		  xmlns:xlink="http://www.w3.org/1999/xlink"
		  xmlns:svg="http://www.w3.org/2000/svg"
		  xmlns:mml="http://www.w3.org/1998/Math/MathML"
		  xmlns:db="http://docbook.org/ns/docbook"
		  version="5.0-subset Scilab"
		  xml:lang="en"
		  xml:id="mminfo">
  <info>
    <pubdate>18-Apr-2011</pubdate>
  </info>
  <refnamediv>
    <refname>mminfo</refname>
    <refpurpose> Extracts size and storage information out of a Matrix Market file</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
	[rows,cols,entries,rep,field,symm,comments] = mminfo(filename)
	</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>

      <varlistentry>
        <term>filename</term>
        <listitem>
          <para>
            a 1-by-1 matrix of strings, path of the Matrix Market file, 
            or logical unit associated to that file.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rows</term>
        <listitem>
          <para>
            a 1-by-1 real matrix, number of matrix rows.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>cols</term>
        <listitem>
          <para>
            a 1-by-1 real matrix, number of matrix columns.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>entries</term>
        <listitem>
          <para>
            a 1-by-1 real matrix, number of entries stored in the file (see below).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rep</term>
        <listitem>
          <para>
            a 1-by-1 matrix of strings, with values "coordinate" or "array".
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>field</term>
        <listitem>
          <para>
            a 1-by-1 matrix of strings, with values "real",  "complex" or "pattern".
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>symm</term>
        <listitem>
          <para>
            a 1-by-1 matrix of strings, with values "general", "symmetric", "skew-symmetric", "hermitian".
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>comments</term>
        <listitem>
          <para>
            a 1-by-n matrix of strings, contains the comments present in the file header.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
        <para>
          <literal>mminfo</literal>
          Extracts size and storage information out of a Matrix Market file.
        </para>
        <para>
          In the case of coordinate matrices, entries refers to the
          number of coordinate entries stored in the file.  The number
          of non-zero entries in the final matrix cannot be determined
          until the data is read (and symmetrized, if necessary).

          In the case of array matrices, entries is the product
          rows*cols, regardless of whether symmetry was used to
          store the matrix efficiently.
        </para>
        <para>
          If <literal>filename</literal> is a file path ,
          <literal>mminfo(filename)</literal> opens the file, reads the information
          and then closes the file.

        </para>
        <para>
          If <literal>filename</literal> is a logical unit (see <literal>mopen</literal>) ,
          <literal>mminfo(filename)</literal>  reads the information and and let the
          file pointer positionned at the beginning of the data in the file
          without left hand side arguments <literal>mminfo(filename)</literal>
          displays the file information.
        </para>
  </refsection>

  <refsection>
    <title>Examples</title>
        <para>
          In the following example, we extract the information from 
		  a 4-by-4 complex hermitian matrix.
        </para>
    <programlisting role="example">
		<![CDATA[
		filename = fullfile(mmgetpath(),"tests","unit_tests","A.mtx");
		[rows,cols,entries,rep,field,symm,comments] = mminfo(filename)
		]]>
    </programlisting>
        <para>
          In the following example, we extract the information 
		  from a file descriptor, as the output of mopen.
        </para>
    <programlisting role="example">
		<![CDATA[
fd = mopen(filename,"r");
[rows,cols,entries,rep,field,symm,comments] = mminfo(fd);
mclose(fd);
		]]>
    </programlisting>
        <para>
          In the following example, we analyze a collection of files 
		  downloaded from http://math.nist.gov/MatrixMarket.
        </para>
    <programlisting role="example">
		<![CDATA[
path = fullfile(mmgetpath (  ),"tests","unit_tests");
F=listfiles(fullfile(path,"*.mtx"));
for filename=F'
  [rows,cols,entries,rep,field,symm,comments] = mminfo(filename);
  mprintf("%-4d-by-%-4d, nonzeros: %-6d, %-15s, %-15s, %-15s \n",rows,cols,entries,rep,field,symm);
end
		]]>
    </programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
		<member>
			<link linkend="mmread"> mmread</link>
		</member>
		<member>
			<link linkend="mmwrite"> mmwrite</link>
		</member>
		<member>
			<link linkend="mmgetpath"> mmgetpath</link>
		</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

	<para>
		Copyright (C) 2009 - INRIA - Serge Steer
	</para>
	
	<para>
		Copyright (C) 2011 - Benoit Goepfert
	</para>

	<para>
		Copyright (C) 2011 - DIGITEO - Michael Baudin
	</para>

  </refsection>

  <refsection>
    <title>Bibliography</title>
    <para>
      http://math.nist.gov/MatrixMarket/formats.html
    </para>
    <para>
	http://math.nist.gov/MatrixMarket/mmio/matlab/mminfo.m
    </para>
  </refsection>

</refentry>
